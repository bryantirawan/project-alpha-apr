from django.urls import reverse_lazy, reverse
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from tasks.forms import TaskForm
from tasks.models import Task
from projects.models import Project
from django.db.models import OuterRef
from django.contrib.auth import get_user_model
from django.db.models import Q


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"

    form_class = TaskForm

    def get_form_kwargs(self):
        kwargs = super(TaskCreateView, self).get_form_kwargs()
        kwargs["user"] = self.request.user

        # Attempt #1: assignee list has list of projects
        # when trying to print Project.values only name and description show up. Maybe user is protected?
        # kwargs["project_members"] = Project.objects.values_list(
        #     "pk", flat=Truep
        # )

        # Attempt #2: nothing shows up in assignee list
        # kwargs["project_id"] = Project.objects.all()[0].members.name
        # print(Project.objects.all()[0].members)  # prints auth.User.none
        return kwargs

    # stackoverflow attempt
    # def get_form(self, *args, **kwargs):
    #     form = super(TaskCreateView, self).get_form(*args, **kwargs)
    #     form.fields["assignee"].queryset = Project.members.filter(
    #         project_id=self.kwargs["project_id"]
    #     )

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/mytaskslist.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskSearchListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/search.html"

    def get_queryset(self):
        query = self.request.GET.get("q")
        object_list = Task.objects.filter(
            Q(name__icontains=query), assignee=self.request.user
        )
        return object_list


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")

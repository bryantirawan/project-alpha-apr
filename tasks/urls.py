from django.urls import path
from tasks.views import (
    TaskCreateView,
    TaskListView,
    TaskSearchListView,
    TaskUpdateView,
)

urlpatterns = [
    path(
        "create/<int:project_id>", TaskCreateView.as_view(), name="create_task"
    ),
    path("mine/", TaskListView.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
    path("search/", TaskSearchListView.as_view(), name="search_taskresults"),
]

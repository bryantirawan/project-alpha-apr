from django.forms import ModelForm
from tasks.models import Task
from projects.models import Project
from django.contrib.auth.models import User


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        # project_members = kwargs.pop("project_members")
        super(TaskForm, self).__init__(*args, **kwargs)
        self.fields["project"].queryset = Project.objects.filter(members=user)
        # self.fields["assignee"].queryset = Project.objects.filter(
        #     members=project_members[0]
        # )

    # stackoverflow attempt
    # def form_valid(self, form):
    #     form.instance.project_id = Project.self.kwargs["project_id"]
    #     return super(TaskForm, self).form_valid(form)

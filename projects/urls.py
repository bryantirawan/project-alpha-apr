from projects.views import (
    ProjectCreateView,
    ProjectDetailView,
    ProjectListView,
    ProjectSearchResultView,
)

from django.urls import path

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
    path("search/", ProjectSearchResultView.as_view(), name="search_results"),
]

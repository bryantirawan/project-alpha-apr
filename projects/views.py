from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from projects.models import Project
from tasks.models import Task
from django.db.models import Exists, OuterRef, Q

# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        queryset = Project.objects.filter(members=self.request.user)
        # tested above to see if this will filter projects based off of user and it does work
        subquery = Task.objects.filter(
            project=OuterRef("pk"), is_completed=False
        )
        queryset = queryset.annotate(all_completed=~Exists(subquery))
        return queryset


class ProjectSearchResultView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/search.html"

    def get_queryset(self):
        query = self.request.GET.get("q")
        object_list = Project.objects.filter(
            Q(name__icontains=query), members=self.request.user
        )
        return object_list


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"
    context_object_name = "project"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
